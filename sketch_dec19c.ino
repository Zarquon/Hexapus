#include <DS3231.h>

DS3231  rtc(SDA, SCL);

Time  t;


#include <Servo.h>

Servo myservo;
Servo myservo1;
Servo myservo2;
Servo myservo3;
Servo myservo4;
Servo myservo5;
Servo myservo6;

int pos = 90;

void setup()
{
  // Setup Serial connection
  Serial.begin(115200);

  // Initialize the rtc object
  rtc.begin();
  
  // The following lines can be uncommented to set the date and time
  //rtc.setDOW(WEDNESDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(9, 35, 40);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(1, 1, 2014);   // Set the date to January 1st, 2014

  pinMode(9,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(11,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(A3,OUTPUT);
  pinMode(A0,OUTPUT);
  pinMode(A1,OUTPUT);
  digitalWrite(A0,HIGH);  
  digitalWrite(A1,HIGH);

  myservo.attach(2); 
  myservo1.attach(3); 
  myservo2.attach(4); 
  myservo3.attach(5); 
  myservo4.attach(6); 
  myservo5.attach(7); 
  myservo6.attach(8);
}

void loop()
{
  // Get data from the DS3231
  t = rtc.getTime();
  

  // Send Day-of-Week and time
 
  Serial.print(t.hour, DEC);
  Serial.print(" hour(s), ");
  Serial.print(t.min, DEC);
  Serial.print(" minute(s) and ");
  Serial.println(t.sec, DEC);


  if (t.sec == 0) {
    digitalWrite(9,HIGH);
    digitalWrite(10,HIGH);
    digitalWrite(11,HIGH);
    digitalWrite(12,HIGH);
    digitalWrite(13,HIGH);
    digitalWrite(A3,HIGH);
    digitalWrite(A0,LOW);
    delay(17000);
    digitalWrite(A0,HIGH);
        

    for (int x = 0; x < 12; x++) {
    for (pos = 60; pos <= 120 ; pos += 1) { 
      myservo.write(pos);  
      myservo1.write(pos); 
      myservo2.write(pos);
      myservo3.write(pos); 
      myservo4.write(pos);  
      myservo5.write(pos); 
      myservo6.write(pos);      
      delay(15);                       
    }
    for (pos = 120; pos >= 60; pos -= 1) { 
      myservo.write(pos);  
      myservo1.write(pos); 
      myservo2.write(pos);
      myservo3.write(pos); 
      myservo4.write(pos);  
      myservo5.write(pos); 
      myservo6.write(pos);            
      delay(15);                       
    }
    }

    delay(2000);
    digitalWrite(9,LOW);
    digitalWrite(10,LOW);
    digitalWrite(11,LOW);
    digitalWrite(12,LOW);
    digitalWrite(13,LOW);
    digitalWrite(A3,LOW);

  }

  
}
